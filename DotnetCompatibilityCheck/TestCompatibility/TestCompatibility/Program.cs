﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace TestCompatibility
{
    class Program
    {
        static void Main(string[] args)
        {
            var (a,b,c) = (Operation(5, 20).Result);
            Console.WriteLine(a + " " +b+" "+ c);
            var res = add(5, 10).Result;
            Console.WriteLine(res);
            Console.ReadKey();
        }

        ////ValueTuple  Must use C# 7 and dot.net 4.7.2
        public static async Task<(int, int, int)> Operation(int a, int b)
        {
            var res = add(a, b).Result;
            return (a, b, res);
        }

        public static async Task<int> add(int a, int b)
        {
            return await Task.FromResult(a + b);
        }
    }
}
